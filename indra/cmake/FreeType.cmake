# -*- cmake -*-
include(Prebuilt)

if (STANDALONE OR LINUX)
  include(FindPkgConfig)

  pkg_check_modules(FREETYPE REQUIRED freetype2)
else (STANDALONE OR LINUX)
  use_prebuilt_binary(freetype)
  set(FREETYPE_INCLUDE_DIRS ${LIBS_PREBUILT_DIR}/include/freetype2)
  set(FREETYPE_LIBRARIES freetype)
endif (STANDALONE OR LINUX)

link_directories(${FREETYPE_LIBRARY_DIRS})
